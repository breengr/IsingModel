#!usr/bin/env python 

"""
Created on Mon Jan 08 20:44:31 2018

@author: Gracen
"""
import numpy as np 
import matplotlib.pyplot as plt
import random 
import math

#initialized values
N = 25
sweeps_no = 500
sw = float(500.0) #float value of sweeps_no required when calculating the average magnetization

 #T has 20 elements

kb = 1.0 # setting Boltzmann Constant to 1 for simplicity of calculations
J = 1.0
#dividing factor. Used to determine average energy and magnetization after summing through lattice
z1 = 1.0/(sweeps_no*N*N) 
z2 = 1.0/(sweeps_no*sweeps_no*N*N)

def Lattice(N):
    '''Creates an NxN lattice with randomly orientated spins'''
    lattice = np.random.rand(N,N)
    lattice = np.where(lattice <= 0.5, 1,-1)
    return lattice
lattice = Lattice(N)

def EnergyCal(lattice):
    '''Calculating the energy of the lattice'''
    Energy = 0
    for i in range(len(lattice)):
        for j in range(len(lattice)):
            site = lattice[i,j]
            neighbours = lattice[(i - 1) % N, j] + lattice[(i + 1) % N, j] + \
       lattice[i, (j - 1) % N] + lattice[i, (j + 1) % N] #spin state of neighbours
            Energy += -neighbours*site 
    #print Energy
    return Energy/4. #prevents counting multiple times

def magnetization(lattice):
    '''computes the magnetization of all sites in the lattice'''
    mag = np.sum(lattice)
    return mag

J = 1.0
n,n2 = lattice.shape
E1 = M1 = 0
Energylist = []
Magnetlist = []
sumarray = []



      
for temp in np.linspace(1.0,4.0,num=100):   
    magnetarray = []     
    magnetarray2 = [] #for calculating magnetic susceptibility
    energyarray = []
    E_sqr_array = []
    E1 = M1 = M2 = 0
    for step in np.arange(0, sw*2):    
    #for step in range(sweeps_no):        
        for i in range(n-1):
           for j in range(n-1): 
              # here should be the problem, you have to make the difference of energies before and after the flip!
            
            H_old = -J*lattice[i,j]*(lattice[i,j-1] + lattice[i,j+1] + lattice[i-1,j] + lattice[i+1,j])
            H_new = J*lattice[i,j]*(lattice[i,j-1] + lattice[i,j+1] + lattice[i-1,j] + lattice[i+1,j])   
            del_H = H_old-H_new
            if del_H >= 0:
                lattice[i,j] = -lattice[i,j]
            elif del_H < 0:
                    prob = np.exp((del_H)/(temp))   
                    rand = random.random()
                    if rand < prob:
                        lattice[i,j] = -lattice[i,j]
                    else:
                        lattice[i,j] = lattice[i,j]  
                    
            
            
        if step >= (sw):
            #magnetization values taken after equilibrium reached
#            Mag = (magnetization(lattice))
#            magnetarray.append(Mag)
            
#            Mag_sqr = (magnetization(lattice))**2
#            magnetarray2.append(Mag_sqr) #magnet squared
            
            #energy values taken after equilibrium reached
            Ene = EnergyCal(lattice)
            energyarray.append(Ene)
            #print 'energy', energyarray
            
            Ene_sqr = (EnergyCal(lattice))**2
            E_sqr_array.append(Ene_sqr)
        step += 1 
            
        

    print 'temp', temp
    energy_sum = []
    energy_sum = sum(energyarray)
 #   print 'energy sum', energy_sum
    energysqr_sum = sum(E_sqr_array)
    
    ''' When calculating the average Energy and average Magnetization per site a factor of 2 is included as only half the 
    amount of sweeps were used as the algorithim waits until equilibrium has been reached before taken values for energy and 
    magnetization in order to generate accurate plots'''
    energydata = (energy_sum)/(N*N*sw) 
    energydata2 = (energysqr_sum)/(N*N*sw*temp**2)
    energydata3 = ((energy_sum)/(N*sw))**2*((1/temp)**2)
    specific_heat = abs(energydata2 - energydata3)
    
#    magnet_sum =[]
#    magnet_sum = sum(magnetarray)
#    magnet_sum2 =[]
 #   magnet_sum2 = np.average(magnetarray2)
 #   magnet_sum2 = sum(magnetarray2)
#    print 'magnet sum', magnet_sum
#    magnet_data = (abs(magnet_sum))/(N*N*sw)
    
#    magnet_data2 = (magnet_sum2)/(sw*N*N*temp)
#    magnet_data3 = (((magnet_sum)/(N*sw))**2)*(1/(temp))
#    magnet_suscep = abs(magnet_data2 - magnet_data3)
    
    
    
    
    plt.figure(10)
    plt.plot(temp, specific_heat, 'ro')
   
    #plt.plot(T, energyarray, 'o')
    
    '''magnetic susceptibility is how the magnetization changes with an applied magnetic field.
    in other words, '''
    
    
    

#plt.plot(T, energyarray)
plt.xlabel(r'$T, (J / k_\beta)$')
#plt.ylabel(r'$ \chi (\mu / k_\beta)$')

#plt.ylabel(r'$ <M> per site, \mu$' )
#plt.ylabel(r'$ <E> per site, (J)')
plt.ylabel(r'$ C_v k_\beta $')
#plt.title("Magnetic Susceptibility as a Function of Temperature")
#plt.title("Average Magnetization per Site as a Function of Temperature")


plt.show()


            #Magnetization
#plt.plot(T, Energy, 'o')
#plt.xlabel("Temperature")
#plt.ylabel("Energy")

#    mag = Magnetization(lat)
#    mag[sweeps_no] = abs(sum(sum(lat))) / (N ** 2)
#        print(temperature, sum(mag[sweeps]) / sweeps)
