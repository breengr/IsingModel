#!/usr/bin/env python 

"""
Created on Sat Jan 13 13:07:35 2018

@author: Gracen
"""

'''Determing the Ground State of a Triangular Lattice'''


import numpy as np 
import matplotlib.pyplot as plt
import random 
import math

#initialized values
N = 50
sweeps_no = 500
sw = float(500.0) #float value of sweeps_no required when calculating the average magnetization

 #T has 20 elements

kb = 1.0 # setting Boltzmann Constant to 1 for simplicity of calculations
J = 1.0
#dividing factor. Used to determine average energy and magnetization after summing through lattice
z1 = 1.0/(sweeps_no*N*N) 
z2 = 1.0/(sweeps_no*sweeps_no*N*N)

def Lattice(N):
    '''Creates an NxN lattice with randomly orientated spins'''
    lattice = np.random.rand(N,N)
    lattice = np.where(lattice <= 0.5, 1,-1)
    return lattice
lattice = Lattice(N)

def EnergyCal(lattice):
    '''Calculating the energy of the lattice'''
    Energy = 0
    for i in range(len(lattice)):
        for j in range(len(lattice)):
            site = lattice[i,j]
            neighbours = (lattice[i, (j+1)%N] + lattice[(i + 1)%N, (j+1)%N] + \
           lattice[(i+1)%N ,j] + lattice[i, (j - 1) % N] + lattice[(i-1)%N, (j-1)%N] + lattice[(i-1)%N, j]) #spin state of neighbours
            Energy += -neighbours*site 
    #print Energy
    return Energy/4. 
def magnetization(lattice):
    '''computes the magnetization of all sites in the lattice'''
    mag = np.sum(lattice)
    return mag

J = 1.0
n,n2 = lattice.shape
E1 = M1 = 0
Energylist = []
Magnetlist = []
sumarray = []

#T = np.linspace(0.5,3.5,num=10)
n1, n2  = 1.0/(sw*N*N), 1.0/(sw*sw*N*N)

      
for temp in np.linspace(0.1,4.0,num=10):   
    magnetarray = []     
    magnetarray2 = [] #for calculating magnetic susceptibility
    energyarray = []
    E_sqr_array = []
    E1 = M1 = M2 = 0
    for step in np.arange(0, sw*2):    
    #for step in range(sweeps_no):        
        for i in range(n-1):
           for j in range(n-1): 
               H_old = -J*lattice[i,j]*(lattice[i, (j+1)%N] + lattice[(i + 1)%N, (j+1)%N] + \
           lattice[(i+1)%N ,j] + lattice[i, (j - 1) % N] + lattice[(i-1)%N, (j-1)%N] + lattice[(i-1)%N, j])
               H_new = J*lattice[i,j]*(lattice[i, (j+1)%N] + lattice[(i + 1)%N, (j+1)%N] + \
           lattice[(i+1)%N ,j] + lattice[i, (j - 1) % N] + lattice[(i-1)%N, (j-1)%N] + lattice[(i-1)%N, j])  
               del_H = H_old-H_new
               if del_H >= 0:
                   lattice[i,j] = -lattice[i,j]
               elif del_H < 0:
                    prob = np.exp((del_H)/(temp))   
                    rand = random.random()
                    if rand < prob:
                        lattice[i,j] = -lattice[i,j]
                    else:
                        lattice[i,j] = lattice[i,j]  
               
    
                    
            
            
        if step >= (sw):

            Ene = EnergyCal(lattice)
            energyarray.append(Ene)
            #print 'energy', energyarray
            
            Ene_sqr = (EnergyCal(lattice))**2
            E_sqr_array.append(Ene_sqr)
        step += 1 
            
        

    print 'temp', temp #included to ensure the algorithm was looping through the temperatures correctly
    energy_sum = []
    energy_sum = sum(energyarray)

    energysqr_sum = sum(E_sqr_array)
    

    energydata = (energy_sum)/(2*N*N*sw) 
    energydata2 = (energysqr_sum)/(N*N*sw*temp**2)
    energydata3 = ((energy_sum)/(N*sw))**2*((1/temp)**2)
    specific_heat = abs(energydata2 - energydata3)

    
    
    
    
    plt.figure(10)
    plt.plot(temp, energydata, 'ro')
   
    #plt.plot(T, energyarray, 'o')
    
    '''magnetic susceptibility is how the magnetization changes with an applied magnetic field.
    in other words, '''
    
    
    
plt.xlabel(r'$T, (J / k_\beta)$')

plt.ylabel(r'$ <E> per site, (J)$')



plt.show()

