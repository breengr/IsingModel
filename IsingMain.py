#!/usr/bin/env python

"""
Created on Sat Jan 13 14:40:37 2018

@author: Gracen
"""
import numpy as np
import matplotlib.pyplot as plt
import random

#initialised values 
T = float(1.0) #temperature 

N = 25 #lattice dimension
sweeps = 500 #number of sweeps used 

nx = N
ny = N #size of the NxN matrix

#Ising code

class Ising:
    """creating the ising model in this class"""
    
    def __init__(self, nx, ny, random=False):
        '''creating a function which creates an initial matrix with randomly 
        orientated spins of 1 and -1 '''
        self._lattice = np.random.choice(np.array([-1,1]),size=(N,N))


    def metropolis(self, lattice, x, y):
        '''Performs the monte carlo using the metropolis algorithim on the sites of the lattice
        determining whether to flip or not''' 
        J = 1    
 
        n = N #lattice dimension
   
          
        for i in range(n-1):
            for j in range(n-1): 
          #summing throughout the lattice site
        
        
                '''  periodic boundary conditions are applied as we are dealing with a lattice on finite length '''
                H_old = -J*self._lattice[i,j]*(self._lattice[i,(j-1)%N] + self._lattice[i,(j+1)%N] + self._lattice[(i-1)%N,j] \
                + self._lattice[(i+1)%N,j])
                H_new = J*self._lattice[i,j]*(self._lattice[i,(j-1)%N] + self._lattice[i,(j+1)%N] + self._lattice[(i-1)%N,j] \
                + self._lattice[(i+1)%N,j])
               
               
                dE = H_old-H_new
                #flipping if energy change is less than zero
                if dE >= 0:
                            self._lattice[i,j] = -self._lattice[i,j] 
                #energy change greater may also be flipped if the probability is less than 
                #randomly generated number
                            
                elif dE < 0:
                    prob = np.exp((dE)/(T))   
                    rand = random.random()
                    if rand < prob:
                        self._lattice[i,j] = -self._lattice[i,j]
                    else:
                        self._lattice[i,j] = self._lattice[i,j]
        return self._lattice
               
        
    def show(self, fig, lattice, t, N):  
        '''showing the lattice graphically'''
        fig = plt.figure()    
        image = plt.imshow(self._lattice, cmap='gray')
        fig.canvas.draw()
        plt.show()
    
    
    def simulate(self):   
            ''' Sweeping through the Ising model with the metropolis algorithim previously defined'''
            #N, temp     = 50, 100        # Initialse the lattice
            lattice = Ising(nx,ny,random=True)
            #lattice2 = np.array(lattice)
            fig = plt.figure(figsize=(15, 15), dpi=80);    
            self.show(fig, lattice, 0, N);
            
            
            for t in range(sweeps):
                self.metropolis(lattice, N, 1.0/T) 
                self.show(fig, lattice, t, N); #showing the lattice after flipping 
    
#executing
rm = Ising(25,25) 
rm.simulate()       
